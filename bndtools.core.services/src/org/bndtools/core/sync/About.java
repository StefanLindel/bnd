package org.bndtools.core.sync;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class About {
	final static Logger logger = LoggerFactory.getLogger("bndtools.central.sync");
}
